﻿using UnityEngine;

public static class ImageFilter
{
    public static Texture2D Multiply(Texture2D texture, float multiplier)
    {
        int width = texture.width;
        int height = texture.height;

        Color[] colors = texture.GetPixels(0, 0, width, height);

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                float grayscale = colors[width * x + y].grayscale * multiplier;
                colors[width * x + y] = new Color(grayscale, grayscale, grayscale);
            }
        }

        texture.SetPixels(colors);
        texture.Apply();

        return texture;
    }

    public static Texture2D Invert(Texture2D texture)
    {
        int width = texture.width;
        int height = texture.height;

        Color[] colors = texture.GetPixels(0, 0, width, height);

        for (int x = 0; x < texture.width; x++)
        {
            for (int y = 0; y < texture.height; y++)
            {
                float grayscale = Invert(colors[width * x + y].grayscale);
                colors[width * x + y] = new Color(grayscale, grayscale, grayscale);
            }
        }

        texture.SetPixels(colors);
        texture.Apply();

        return texture;
    }

    public static Texture2D Avg01(Texture2D texture)
    {
        int width = texture.width;
        int height = texture.height;

        Color[] colors = texture.GetPixels(0, 0, width, height);

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                float grayscale = Avg01(colors[width * x + y].grayscale);
                colors[width * x + y] = new Color(grayscale, grayscale, grayscale);
            }
        }

        texture.SetPixels(colors);
        texture.Apply();

        return texture;
    }

    public static Texture2D Offset(Texture2D texture, Vector2Int offset)
    {
        if (offset == Vector2.zero)
        {
            return texture;
        }

        int width = texture.width;
        int height = texture.height;

        Texture2D outputTexture = new Texture2D(width, height);
        Color[] inputColors = texture.GetPixels();
        Color[] outputColors = new Color[width * height];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                int colorIndex = width * x + y;

                int newX = x - offset.x;
                int newY = y - offset.y;

                if (newX > 0 && newX < texture.width && newY > 0 && newY < texture.height)
                {
                    outputColors[colorIndex] = inputColors[width * newX + newY];
                }
                else
                {
                    outputColors[colorIndex] = Color.black;
                }
            }
        }

        outputTexture.SetPixels(outputColors);
        outputTexture.Apply();

        return outputTexture;
    }

    public static float Invert(float graysacale)
    {
        return 1f - graysacale;
    }

    public static float Avg01(float graysacale)
    {
        return graysacale < 0.5f ? 0f : 1f;
    }
}