﻿using UnityEngine;
using System;

public static class ImageBlender
{
    public static Texture2D BlendTextures(Texture2D texture1, Texture2D texture2, BlendMode blendMode)
    {
        int width = texture1.width;
        int height = texture2.height;

        Color[] colors1 = texture1.GetPixels(0, 0, width, height);
        Color[] colors2 = texture2.GetPixels(0, 0, width, height);
        Color[] outputColors = new Color[width * height];

        Texture2D outputTexture = new Texture2D(width, height);

        Func<float, float, float> blendFunc = GetBlendFunc(blendMode);

        Color tempColor = Color.black;
        float grayscale = 1f;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                int colorIndex = width * x + y;

                grayscale = blendFunc(
                    colors1[colorIndex].grayscale,
                    colors2[colorIndex].grayscale);

                tempColor.r = grayscale;
                tempColor.g = grayscale;
                tempColor.b = grayscale;

                outputColors[colorIndex] = tempColor;
            }
        }

        outputTexture.SetPixels(outputColors);
        outputTexture.Apply();

        return outputTexture;
    }

    private static Func<float, float, float> GetBlendFunc(BlendMode blendMode)
    {
        switch (blendMode)
        {
            case BlendMode.Normal: return Normal;
            case BlendMode.Darken: return Darken;
            case BlendMode.Multiply: return Multiply;
            case BlendMode.ColorBurn: return ColorBurn;
            case BlendMode.LinearBurn: return LinearBurn;
            case BlendMode.Lighten: return Lighten;
            case BlendMode.Screen: return Screen;
            case BlendMode.ColorDodge: return ColorDodge;
            case BlendMode.LinearDodge: return LinearDodge;
            case BlendMode.Overlay: return Overlay;
            case BlendMode.SoftLight: return SoftLight;
            case BlendMode.HardLight: return HardLight;
            case BlendMode.VividLight: return VividLight;
            case BlendMode.LinearLight: return LinearLight;
            case BlendMode.PinLight: return PinLight;
            case BlendMode.Difference: return Difference;
            case BlendMode.Exclusion: return Exclusion;
            default: return Normal;
        }
    }

    public static float Normal(float graysacale1, float graysacale2)
    {
        return graysacale1;
    }

    public static float Darken(float graysacale1, float graysacale2)
    {
        return Mathf.Min(graysacale1, graysacale2);
    }

    public static float Multiply(float graysacale1, float graysacale2)
    {
        return graysacale1 * graysacale2;
    }

    public static float ColorBurn(float graysacale1, float graysacale2)
    {
        return 1f - (1f - graysacale1) / graysacale2;
    }

    public static float LinearBurn(float graysacale1, float graysacale2)
    {
        return graysacale1 + graysacale2 - 1f;
    }

    public static float Lighten(float graysacale1, float graysacale2)
    {
        return Mathf.Max(graysacale1, graysacale2);
    }

    public static float Screen(float graysacale1, float graysacale2)
    {
        return 1f - (1f - graysacale1) * (1f - graysacale2);
    }

    public static float ColorDodge(float graysacale1, float graysacale2)
    {
        return graysacale1 / (1f - graysacale2);
    }

    public static float LinearDodge(float graysacale1, float graysacale2)
    {
        return graysacale1 + graysacale2;
    }

    public static float Overlay(float graysacale1, float graysacale2)
    {
        return
            Convert.ToSingle(graysacale1 > 0.5f) * (1f - (1f - 2f * (graysacale1 - 0.5f)) * (1f - graysacale2))
            +
            Convert.ToSingle(graysacale1 <= 0.5f) * (2f * graysacale1 * graysacale2);
    }

    public static float SoftLight(float graysacale1, float graysacale2)
    {
        return
            Convert.ToSingle(graysacale2 > 0.5f) * (1f - (1f - graysacale1) * (1f - (graysacale2 - 0.5f)))
            +
            Convert.ToSingle(graysacale2 <= 0.5f) * (graysacale1 * (graysacale2 + 0.5f));
    }

    public static float HardLight(float graysacale1, float graysacale2)
    {
        return
            Convert.ToSingle(graysacale2 > 0.5f) * (1f - (1f - graysacale1) * (1f - 2f * (graysacale2 - 0.5f)))
            +
            Convert.ToSingle(graysacale2 <= 0.5f) * (graysacale1 * (2f * graysacale2));
    }

    public static float VividLight(float graysacale1, float graysacale2)
    {
        return
            Convert.ToSingle(graysacale2 > 0.5f) * (1f - (1f - graysacale1) / (2f * (graysacale2 - 0.5f)))
            +
            Convert.ToSingle(graysacale2 <= 0.5f) * (graysacale1 / (1f - 2f * graysacale2));
    }

    public static float LinearLight(float graysacale1, float graysacale2)
    {
        return
            Convert.ToSingle(graysacale2 > 0.5f) * (graysacale1 + 2f * (graysacale2 - 0.5f))
            +
            Convert.ToSingle(graysacale2 <= 0.5f) * (graysacale1 + 2f * graysacale2 - 1f);
    }

    public static float PinLight(float graysacale1, float graysacale2)
    {
        return
            Convert.ToSingle(graysacale2 > 0.5f) * Mathf.Max(graysacale1, 2f * (graysacale2 - 0.5f))
            +
            Convert.ToSingle(graysacale2 <= 0.5f) * Mathf.Min(graysacale1, 2f * graysacale2);
    }

    public static float Difference(float graysacale1, float graysacale2)
    {
        return Mathf.Abs(graysacale1 - graysacale2);
    }

    public static float Exclusion(float graysacale1, float graysacale2)
    {
        return 0.5f - 2f * (graysacale1 - 0.5f) * (graysacale2 - 0.5f);
    }
}