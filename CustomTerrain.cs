﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;

[ExecuteInEditMode]
public class CustomTerrain : MonoBehaviour
{
    public bool lockInspector;

    public TerrainData terrainData;
    public List<TerrainMap> terrainMaps = new List<TerrainMap>();

    [System.Serializable]
    public class Options
    {
        [Header("Generator Options")]
        public bool heightsGeneration = true;
        [Range(0, 50)]
        public int smoothPower = 1;
        public bool detailGeneration = true;
        public bool vegetationGeneration = true;
        public bool texturesGeneration = true;
        public bool debugTime = false;

        [Header("Unity Texture Importer Settings")]
        public bool importerIsReadable;
        public bool importerTextureTypeSprite;
        public int importerAnisoLevel = 1;
    }

    public Options options;

    [HideInInspector]
    public Texture2D mapForBlend1;
    [HideInInspector]
    public Texture2D mapForBlend2;
    [HideInInspector]
    public Texture2D mapBlended;
    [HideInInspector]
    public BlendMode blendMode = BlendMode.Normal;
    [HideInInspector]
    public bool mapAvg01 = false;
    [HideInInspector]
    public bool mapInverted = false;
    [HideInInspector]
    public string mapExportName = "CustomMap";
    [HideInInspector]
    [Range(0f, 10f)]
    public float grayscaleMultiplier = 1f;

    #region Map Blender

    public void BlendMaps()
    {
        if (mapForBlend1 != null && mapForBlend2 != null)
        {
            if (mapForBlend1.width == mapForBlend2.width && mapForBlend1.height == mapForBlend2.height)
            {
                mapBlended = ImageBlender.BlendTextures(mapForBlend1, mapForBlend2, blendMode);

                if (grayscaleMultiplier != 1f)
                {
                    ImageFilter.Multiply(mapBlended, grayscaleMultiplier);
                }

                if (mapAvg01)
                {
                    ImageFilter.Avg01(mapBlended);
                }

                if (mapInverted)
                {
                    ImageFilter.Invert(mapBlended);
                }
            }
        }
    }

    public void ExportBlendedMaps()
    {
        if (mapBlended != null)
        {
            byte[] bytes = mapBlended.EncodeToPNG();
            string name = mapExportName + ".png";
            string path = Application.dataPath + "/Resources/TerrainMaps/" + name;
            File.WriteAllBytes(path, bytes);

            AssetDatabase.Refresh();
        }
    }

    #endregion

    #region Terrain Generator

    public void Generate()
    {
        var realtimeSinceStartup = Time.realtimeSinceStartup;

        ApplyChanges();

        if (options.heightsGeneration == true)
            GenerateHeights();

        Smooth();

        if (options.detailGeneration == true)
            GenerateDetail();

        if (options.vegetationGeneration == true)
            GenerateVegetation();

        if (options.texturesGeneration == true)
            GenerateTextures();

        if (options.debugTime)
            Debug.Log("Generation time: " + (Time.realtimeSinceStartup - realtimeSinceStartup).ToString());
    }

    public void AddTerrainMap()
    {
        terrainMaps.Add(new TerrainMap());
    }

    private void GenerateHeights()
    {
        TerrainGenerator.GenerateHeights(
            terrainData, 
            TerrainMapList.FindNotEmptyMaps(terrainMaps, TerrainMapList.MapType.height));
    }

    private void Smooth()
    {
        TerrainGenerator.Smooth(terrainData, options.smoothPower);
    }

    private void GenerateDetail()
    {
        TerrainGenerator.GenerateDetail(
            terrainData, 
            TerrainMapList.FindNotEmptyMaps(terrainMaps, TerrainMapList.MapType.detail));
    }

    private void GenerateVegetation()
    {
        TerrainGenerator.GenerateVegetation(
            terrainData, 
            TerrainMapList.FindNotEmptyMaps(terrainMaps, TerrainMapList.MapType.vegetation));
    }

    private void GenerateTextures()
    {
        TerrainLayer[] terrainLayers = TerrainGenerator.GenerateTextures(
            terrainData, 
            TerrainMapList.FindNotEmptyMaps(terrainMaps, TerrainMapList.MapType.textures));

        for (int i = 0; i < terrainLayers.Length; i++)
        {
            string name = i.ToString();
            string path = "Assets/Resources/TerrainLayers/" + name + ".terrainlayer";
            AssetDatabase.CreateAsset(terrainLayers[i], path);
        }

        terrainData.terrainLayers = terrainLayers;
    }

    private void ApplyChanges()
    {
        for (int i = 0; i < terrainMaps.Count; i++)
        {
            terrainMaps[i].ApplyChanges();
        }
    }

    #endregion 
}