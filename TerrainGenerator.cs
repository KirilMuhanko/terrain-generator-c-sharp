﻿using System.Collections.Generic;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
    public static void GenerateHeights(TerrainData terrainData, List<TerrainMap> terrainMaps)
    {
        float[,] vertexMap = new float[terrainData.heightmapWidth, terrainData.heightmapHeight];

        for (int i = 0; i < terrainMaps.Count; i++)
        {
            for (int x = 0; x < terrainData.heightmapWidth; x++)
            {
                for (int z = 0; z < terrainData.heightmapHeight; z++)
                {
                    float grayscale = terrainMaps[i].GetHeightMapPixel(x, z);

                    if (grayscale > vertexMap[x, z])
                    {
                        vertexMap[x, z] = grayscale;
                    }
                }
            }
        }

        terrainData.SetHeights(0, 0, vertexMap);
    }

    public static void Smooth(TerrainData terrainData, int power)
    {
        power = Mathf.Clamp(power, 0, 100);

        int mapWidth = terrainData.heightmapWidth;
        int mapHeight = terrainData.heightmapHeight;

        float[,] heights = terrainData.GetHeights(0, 0, mapWidth, mapHeight);

        while (power > 0)
        {
            heights = terrainData.GetHeights(0, 0, mapWidth, mapHeight);
            float[,] newHeights = new float[mapWidth, mapHeight];

            for (int x = 0; x < mapWidth; x++)
            {
                for (int y = 0; y < mapHeight; y++)
                {
                    float sum = 0f;
                    int sumCount = 0;

                    if ((x + 1) < mapWidth)
                    {
                        sum += heights[x + 1, y];
                        sumCount++;

                        if ((y - 1) > 0)
                        {
                            sum += heights[x + 1, y - 1];
                            sumCount++;
                        }

                        if ((y + 1) < mapHeight)
                        {
                            sum += heights[x + 1, y + 1];
                            sumCount++;
                        }
                    }

                    if ((x - 1) > 0)
                    {
                        sum += heights[x - 1, y];
                        sumCount++;

                        if ((y - 1) > 0)
                        {
                            sum += heights[x - 1, y - 1];
                            sumCount++;
                        }

                        if ((y + 1) < mapHeight)
                        {
                            sum += heights[x - 1, y + 1];
                            sumCount++;
                        }
                    }

                    if ((y + 1) < mapHeight)
                    {
                        sum += heights[x, y + 1];
                        sumCount++;
                    }

                    if ((y - 1) > 0)
                    {
                        sum += heights[x, y - 1];
                        sumCount++;
                    }

                    newHeights[x, y] = (heights[x, y] + (sum / sumCount)) * 0.5f;
                }
            }

            terrainData.SetHeights(0, 0, newHeights);
            power--;
        }
    }

    public static void GenerateDetail(TerrainData terrainData, List<TerrainMap> terrainMaps)
    {
        DetailPrototype[] newDetailPrototypes;
        newDetailPrototypes = new DetailPrototype[terrainMaps.Count];

        int detailIndex = 0;

        foreach (TerrainMap terrainMap in terrainMaps)
        {
            newDetailPrototypes[detailIndex] = new DetailPrototype
            {
                prototypeTexture = terrainMap.detail.prototypeTexture,
                healthyColor = terrainMap.detail.healthyColour,
                dryColor = terrainMap.detail.dryColour,
                minHeight = terrainMap.detail.minHeight,
                maxHeight = terrainMap.detail.maxHeight,
                minWidth = terrainMap.detail.minWidth,
                maxWidth = terrainMap.detail.maxWidth,
                noiseSpread = terrainMap.detail.noiseSpread,
                usePrototypeMesh = false,
                renderMode = DetailRenderMode.GrassBillboard
            };
            detailIndex++;
        }

        terrainData.detailPrototypes = newDetailPrototypes;

        for (int i = 0; i < terrainData.detailPrototypes.Length; i++)
        {
            int[,] detailMap = new int[terrainData.detailWidth, terrainData.detailHeight];
            Detail detail = terrainMaps[i].detail;
            detail.spacing = Mathf.Clamp(detail.spacing, 1, 100);

            for (int x = 0; x < terrainData.detailWidth; x += detail.spacing)
            {
                for (int y = 0; y < terrainData.detailHeight; y += detail.spacing)
                {
                    if (terrainMaps[i].GetDetailMapPixel(x, y) != 0)
                    {
                        detailMap[x, y] = 1;
                    }
                }
            }

            terrainData.SetDetailLayer(0, 0, i, detailMap);
        }
    }

    public static void GenerateVegetation(TerrainData terrainData, List<TerrainMap> terrainMaps)
    {
        TreePrototype[] treePrototypes = new TreePrototype[terrainMaps.Count];

        int treeIndex = 0;

        List<TreeInstance> treeInstances = new List<TreeInstance>();

        foreach (TerrainMap terrainMap in terrainMaps)
        {
            treePrototypes[treeIndex] = new TreePrototype
            {
                prefab = terrainMaps[treeIndex].vegetation.prefab,
                bendFactor = 1
            };

            treeIndex++;
        }

        terrainData.treePrototypes = treePrototypes;

        for (int i = 0; i < terrainData.treePrototypes.Length; i++)
        {
            Vegetation vegetation = terrainMaps[i].vegetation;
            int spacing = vegetation.spacing;
            int prevTreeInstancesCount = treeInstances.Count;

            for (int x = 0; x < terrainData.size.x; x += spacing)
            {
                for (int z = 0; z < terrainData.size.z; z += spacing)
                {
                    if (terrainMaps[i].GetVegetationMapPixel(z, x) == 0)
                    {
                        continue;
                    }

                    TreeInstance treeInstance = new TreeInstance();

                    float treePosX = (x + Random.Range(-2f, 2f)) / terrainData.size.x;
                    float treePosZ = (z + Random.Range(-2f, 2f)) / terrainData.size.z;

                    RaycastHit hit;

                    if (Physics.Raycast(new Vector3(x, 0, z) + new Vector3(0, 1000, 0), -Vector3.up, out hit, 1000))
                    {
                        treeInstance.prototypeIndex = i;

                        float treePosY = (hit.point.y - 0) / terrainData.size.y;
                        treeInstance.position = new Vector3(treePosX, treePosY, treePosZ);
                        float treeScale = Random.Range(vegetation.minScale, vegetation.maxScale);
                        treeInstance.heightScale = treeScale;
                        treeInstance.widthScale = treeScale;
                        treeInstance.rotation = Random.Range(vegetation.minRotation, vegetation.maxRotation);
                        treeInstance.color = Color.Lerp(vegetation.color1, vegetation.color2, Random.Range(0.0f, 1.0f));
                        treeInstance.lightmapColor = vegetation.lightColour;

                        treeInstances.Add(treeInstance);

                        if (treeInstances.Count - prevTreeInstancesCount > vegetation.maxCount)
                        {
                            x = (int)terrainData.size.x;
                            z = (int)terrainData.size.z;
                        }
                    }
                }
            }
        }

        terrainData.treeInstances = treeInstances.ToArray();
    }

    public static TerrainLayer[] GenerateTextures(TerrainData terrainData, List<TerrainMap> terrainMaps)
    {
        TerrainLayer[] terrainLayers = new TerrainLayer[terrainMaps.Count];

        int terrainIndex = 0;

        foreach (TerrainMap terrainMap in terrainMaps)
        {
            terrainLayers[terrainIndex] = new TerrainLayer
            {
                name = terrainMap.splatMap.diffuse.name,
                diffuseTexture = terrainMap.splatMap.diffuse,
                normalMapTexture = terrainMap.splatMap.terrainLayer.normalMap,
                normalScale = terrainMap.splatMap.terrainLayer.normalScale,
                specular = terrainMap.splatMap.terrainLayer.specular,
                metallic = terrainMap.splatMap.terrainLayer.metallic,
                smoothness = terrainMap.splatMap.terrainLayer.smoothness,
                tileSize = terrainMap.splatMap.terrainLayer.tileSize,
                tileOffset = terrainMap.splatMap.terrainLayer.tileOffset
            };
            terrainLayers[terrainIndex].diffuseTexture.Apply(true);
            terrainIndex++;
        }

        terrainData.terrainLayers = terrainLayers;

        float[,,] terrainAlphaMaps = new float[
            terrainData.alphamapWidth,
            terrainData.alphamapHeight,
            terrainData.alphamapLayers];

        for (int x = 0; x < terrainData.alphamapWidth; x++)
        {
            for (int y = 0; y < terrainData.alphamapHeight; y++)
            {
                float[] alphamapLayers = new float[terrainData.alphamapLayers];

                for (int j = 0; j < terrainMaps.Count; j++)
                {
                    alphamapLayers[j] = terrainMaps[j].GetSplatMapPixel(x, y)
                        * 100f
                        * terrainMaps[j].splatMap.alphaMultiplier;
                }

                NormalizeVector(alphamapLayers);

                for (int j = 0; j < terrainMaps.Count; j++)
                {
                    terrainAlphaMaps[x, y, j] = alphamapLayers[j];
                }
            }
        }

        terrainData.SetAlphamaps(0, 0, terrainAlphaMaps);
        return terrainLayers;
    }

    private static void NormalizeVector(float[] v)
    {
        float total = 0f;

        for (int i = 0; i < v.Length; i++)
        {
            total += v[i];
        }

        for (int i = 0; i < v.Length; i++)
        {
            v[i] /= total;
        }
    }
}