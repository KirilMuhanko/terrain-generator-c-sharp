﻿using UnityEngine;

[System.Serializable]
public class Vegetation
{
    public GameObject prefab = null;
    public Texture2D map = null;
    public Color color1 = Color.white;
    public Color color2 = Color.white;
    public Color lightColour = Color.white;
    public float minScale = 0.5f;
    public float maxScale = 1.0f;
    public float minRotation = 0;
    public float maxRotation = 360;
    [Range(1, 100)]
    public int spacing = 10;
    public int maxCount = 500;
}