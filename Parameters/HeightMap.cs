﻿using UnityEngine;

[System.Serializable]
public class HeightMap
{
    public Texture2D map = null;
    [Range(0f, 1f)]
    public float multiplier = 0.5f;
}