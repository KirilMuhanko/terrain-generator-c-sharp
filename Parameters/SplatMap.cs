﻿using UnityEngine;

[System.Serializable]
public class SplatMap
{
    [System.Serializable]
    public class TerrainLayerParameters
    {
        public Texture2D normalMap = null;
        [Range(0f, 10f)]
        public float normalScale = 1f;
        public Color specular = Color.black;
        [Range(0f, 1f)]
        public float metallic = 0f;
        [Range(0f, 1f)]
        public float smoothness = 0f;
        public Vector2 tileSize = new Vector2(50, 50);
        public Vector2 tileOffset = new Vector2(0, 0);
    }

    public Texture2D diffuse = null;
    public Texture2D alphaMap = null;
    [Range(0f, 1f)]
    public float alphaMultiplier = 1f;
    public TerrainLayerParameters terrainLayer = new TerrainLayerParameters();
}