﻿using UnityEngine;

[System.Serializable]
public class Detail
{
    public Texture2D prototypeTexture = null;
    public Texture2D map = null;
    public Color dryColour = Color.white;
    public Color healthyColour = Color.white;
    public float minHeight = 2f;
    public float maxHeight = 5f;
    public float minWidth = 2f;
    public float maxWidth = 5f;
    public float noiseSpread = 0.5f;
    [Range(1, 100)]
    public int spacing = 10;
}