﻿using UnityEngine;

[System.Serializable]
public class TerrainMap
{
    public HeightMap heightMap = new HeightMap();
    public Detail detail = new Detail();
    public Vegetation vegetation = new Vegetation();
    public SplatMap splatMap = new SplatMap();

    public Vector2Int layerOffset = Vector2Int.zero;
    public bool layerInverted = false;
    public bool layerEnabled = true;

    private Color[] heightMapColors;
    private Color[] detailColors;
    private Color[] vegetationColors;
    private Color[] splatMapColors;
    
    public void ApplyChanges()
    {
        if (heightMap.map != null)
            heightMapColors = ImageFilter.Offset(heightMap.map, layerOffset).GetPixels();

        if (detail.map != null)
            detailColors = ImageFilter.Offset(detail.map, layerOffset).GetPixels();

        if (vegetation.map != null)
            vegetationColors = ImageFilter.Offset(vegetation.map, layerOffset).GetPixels();

        if (splatMap.alphaMap != null)
            splatMapColors = ImageFilter.Offset(splatMap.alphaMap, layerOffset).GetPixels();
    }

    public float GetHeightMapPixel(int x, int y)
    {
        float grayscale = heightMapColors[heightMap.map.width * x + y].grayscale * heightMap.multiplier;
        return layerInverted ? ImageFilter.Invert(grayscale) : grayscale;
    }

    public float GetDetailMapPixel(int x, int y)
    {
        float grayscale = detailColors[detail.map.width * x + y].grayscale;
        return layerInverted ? ImageFilter.Invert(grayscale) : grayscale;
    }

    public float GetVegetationMapPixel(int x, int y)
    {
        float grayscale = vegetationColors[vegetation.map.width * x + y].grayscale;
        return layerInverted ? ImageFilter.Invert(grayscale) : grayscale;
    }

    public float GetSplatMapPixel(int x, int y)
    {
        float grayscale = splatMapColors[splatMap.alphaMap.width * x + y].grayscale;
        return layerInverted ? ImageFilter.Invert(grayscale) : grayscale;
    }

    public bool IsHeightMapAvailable()
    {
        return layerEnabled && heightMap.map != null;
    }

    public bool IsDetailMapAvailable()
    {
        return layerEnabled && detail.prototypeTexture != null && detail.map != null;
    }

    public bool IsVegetationMapAvailable()
    {
        return layerEnabled && vegetation.prefab != null && vegetation.map != null;
    }

    public bool IsSplatMapAvailable()
    {
        return layerEnabled && splatMap.diffuse != null && splatMap.alphaMap != null;
    }
}