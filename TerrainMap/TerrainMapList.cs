﻿using System.Collections.Generic;

public static class TerrainMapList
{
    public enum MapType
    {
        height,
        detail,
        vegetation,
        textures
    }

    public static List<TerrainMap> FindNotEmptyMaps(List<TerrainMap> maps, MapType mapType)
    {
        List<TerrainMap> terrainMaps = new List<TerrainMap>();

        for(int i = 0; i < maps.Count; i++)
        {
            switch (mapType)
            {
                case MapType.height:
                    {
                        if (maps[i].IsHeightMapAvailable())
                            terrainMaps.Add(maps[i]);
                    }
                    break;
                case MapType.detail:
                    {
                        if (maps[i].IsDetailMapAvailable())
                            terrainMaps.Add(maps[i]);
                    }
                    break;
                case MapType.vegetation:
                    {
                        if (maps[i].IsVegetationMapAvailable())
                            terrainMaps.Add(maps[i]);
                    }
                    break;
                case MapType.textures:
                    {
                        if (maps[i].IsSplatMapAvailable())
                            terrainMaps.Add(maps[i]);
                    }
                    break;
                default: break;
            }
        }

        return terrainMaps;
    }
}