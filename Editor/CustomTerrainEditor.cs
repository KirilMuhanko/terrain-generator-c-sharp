﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CustomTerrain))]
public class CustomTerrainEditor : Editor
{
    CustomTerrain customTerrain;

    bool showMapBlender;

    SerializedProperty mapForBlend1;
    SerializedProperty mapForBlend2;
    SerializedProperty blendMode;
    SerializedProperty mapInverted;
    SerializedProperty mapExportName;
    SerializedProperty grayscaleMultiplier;
    SerializedProperty mapAvg01;
 
    void OnEnable()
    {
        customTerrain = (CustomTerrain)target;
        mapForBlend1 = serializedObject.FindProperty("mapForBlend1");
        mapForBlend2 = serializedObject.FindProperty("mapForBlend2");
        blendMode = serializedObject.FindProperty("blendMode");
        mapAvg01 = serializedObject.FindProperty("mapAvg01");
        mapInverted = serializedObject.FindProperty("mapInverted");
        mapExportName = serializedObject.FindProperty("mapExportName");
        grayscaleMultiplier = serializedObject.FindProperty("grayscaleMultiplier");
    }

    public override void OnInspectorGUI()
    {
        ActiveEditorTracker.sharedTracker.isLocked = customTerrain.lockInspector;
        CustomImportSettings.importerIsReadable = customTerrain.options.importerIsReadable;
        CustomImportSettings.importerTextureTypeSprite = customTerrain.options.importerTextureTypeSprite;
        CustomImportSettings.importerAnisoLevel = customTerrain.options.importerAnisoLevel;

        DrawDefaultInspector();

        showMapBlender = EditorGUILayout.Foldout(showMapBlender, "Map Blender");

        if (showMapBlender)
        {
            EditorGUILayout.PropertyField(mapForBlend1);
            EditorGUILayout.PropertyField(mapForBlend2);
            EditorGUILayout.PropertyField(blendMode);
            EditorGUILayout.PropertyField(mapAvg01);
            EditorGUILayout.PropertyField(mapInverted);
            EditorGUILayout.PropertyField(mapExportName);
            EditorGUILayout.PropertyField(grayscaleMultiplier);

            GUILayout.Label(customTerrain.mapBlended);

            if (GUILayout.Button("Blend Maps"))
            {
                customTerrain.BlendMaps();
            }

            if (GUILayout.Button("Export to Assets/Resources/TerrainMaps"))
            {
                customTerrain.ExportBlendedMaps();
            }

            serializedObject.ApplyModifiedProperties();
        }
        else
        {
            if (GUILayout.Button("Add Terrain Map"))
            {
                customTerrain.AddTerrainMap();
            }

            if (GUILayout.Button("Generate (⊙_⊙)"))
            {
                customTerrain.Generate();
            }
        }
    }
}