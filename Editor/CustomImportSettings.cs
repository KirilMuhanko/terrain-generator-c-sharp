﻿using UnityEditor;

class CustomImportSettings : AssetPostprocessor
{
    public static bool importerIsReadable = true;
    public static bool importerTextureTypeSprite = true;
    public static int importerAnisoLevel = 1;

    void OnPreprocessTexture()
    {
        TextureImporter importer  = (TextureImporter)assetImporter;
        importer.isReadable = true;
        importer.textureType = TextureImporterType.Sprite;
        importer.anisoLevel = 1;
    }
}